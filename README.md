# Pichincha gestor cuentas

## Descripción
Este proyecto consiste en dos microservicios desarrollados con Spring Boot para la gestión de cuentas de clientes. Los microservicios son:

1. **persona-service**: Gestiona la creación de la entidad cliente con sus respectivos datos.
2. **movimiento-service**: Gestiona la creación de cuentas de un cliente y los movimientos realizados en las mismas, además de generar el estado de cuenta.

Ambos microservicios están configurados para utilizar una base de datos SQL y están preparados para ejecutarse en contenedores Docker o de forma local.

## Estructura del proyecto
ms-pichincha-gestor-cuentas/

├── persona-service/

│ ├── src/

│ ├── build/

│ ├── Dockerfile

│ └── ...

├── movimiento-service/

│ ├── src/

│ ├── build/

│ ├── Dockerfile

│ └── ...

└── Gestor cuentas.postman_collection v2.1.json

|__ docker-compose.yml    

├── Gestor cuentas.postman_collection v2.json

└── BaseDatos.sql

## Prerrequisitos
- Java 17
- Docker
- Base de datos SQL (SQL Server, Mysql, MariaDB, postgress, etc)
- Postman


## Instalación

### Clonar el repositorio
git clone https://gitlab.com/cabejarano1/devsu-gestor-cuentas.git

### Configuración de la base de datos
Modifica los archivos application.properties en ambos microservicios (persona-service y movimiento-service) para apuntar a tu instancia de base de datos.

### Ejemplo de application.properties

spring.application.name=movimiento-service

server.port=8082

spring.datasource.url=jdbc:sqlserver://localhost:1433;databaseName=gestor_cuentas;trustServerCertificate=truetrustServerCertificate=true

spring.datasource.username=sa

spring.datasource.password=password

### Crear la base de datos
Utiliza el script SQL proporcionado en database/schema.sql para crear la estructura de la base de datos y data.sql para poblarla con datos iniciales.

## Ejecutar en modo local

### Ejecutar los microservicios

Para ejecutar cada microservicio localmente, navega al directorio del microservicio y usa el siguiente comando:

java -jar build/libs/persona-service-0.0.1-SNAPSHOT.jar

java -jar build/libs/movimiento-service-0.0.1-SNAPSHOT.jar

## Ejecutar en Docker


### Ejecutar los contenedores
Primero se debe ejecutar los comandos en el directorio de cada microservicio:

gradle clean

gradle build

Para ejecutar los microservicios en contenedores Docker, usa el siguiente comando desde la raíz del proyecto:

docker-compose up --build

