package com.pichincha.persona_service;

import com.pichincha.persona_service.constants.PathConstants;
import com.pichincha.persona_service.dto.request.ClienteDto;
import com.pichincha.persona_service.dto.response.ClienteResponse;
import com.pichincha.persona_service.model.EstadoCliente;
import com.pichincha.persona_service.model.Persona;
import com.pichincha.persona_service.repository.EstadoClienteRepository;
import com.pichincha.persona_service.repository.PersonaRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ClienteIntegrationTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private PersonaRepository personaRepository;

    @Autowired
    private EstadoClienteRepository estadoClienteRepository;

    @Test
    public void testCrearYConsultarCliente() {
        // Crear instancias de las entidades relacionadas
        Persona persona = new Persona();
        persona.setNombre("Juan");
        persona.setGenero("M");
        persona.setEdad(30);
        persona.setIdentificacion("1234567890");
        persona.setDireccion("Calle Falsa 123");
        persona.setTelefono("0987654321");
        persona = personaRepository.save(persona);

        EstadoCliente estadoCliente = new EstadoCliente();
        estadoCliente.setNombre("Activo");
        estadoCliente = estadoClienteRepository.save(estadoCliente);

        // Crear ClienteDto
        ClienteDto clienteDto = new ClienteDto();
        clienteDto.setPassword("password");
        clienteDto.setNombre(persona.getNombre());
        clienteDto.setGenero(persona.getGenero());
        clienteDto.setEdad(persona.getEdad());
        clienteDto.setIdentificacion(persona.getIdentificacion());
        clienteDto.setDireccion(persona.getDireccion());
        clienteDto.setTelefono(persona.getTelefono());
        clienteDto.setIdEstadoCliente(estadoCliente.getId());

        // Realizar la solicitud POST para crear el cliente
        ResponseEntity<ClienteResponse> responseEntity = restTemplate.postForEntity(
                "http://localhost:" + port +"/"+ PathConstants.CLIENTE,
                clienteDto,
                ClienteResponse.class
        );

        // Verificar la respuesta
        assertEquals(200, responseEntity.getStatusCodeValue());

        ClienteResponse clienteResponse = responseEntity.getBody();
        assertNotNull(clienteResponse);
        assertNotNull(clienteResponse.getId());
        assertEquals("Juan", clienteResponse.getNombre());

        // Realizar la solicitud GET para consultar el cliente por ID
        ResponseEntity<ClienteResponse> responseGetEntity = restTemplate.getForEntity(
                "http://localhost:" + port +"/"+ PathConstants.CLIENTE +"/" + clienteResponse.getId(),
                ClienteResponse.class
        );

        // Verificar la resp¿getStatusCodeValue());
        ClienteResponse clienteGetResponse = responseGetEntity.getBody();
        assertNotNull(clienteGetResponse);
        assertEquals(clienteResponse.getId(), clienteGetResponse.getId());
        assertEquals("Juan", clienteGetResponse.getNombre());
    }
}
