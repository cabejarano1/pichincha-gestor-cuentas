package com.pichincha.persona_service.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ClienteTest {

    /**
     * Test de la entidad Cliente
     */
    @Test
    public void testCliente() {
        // Crear instancias de las entidades relacionadas
        Persona persona = new Persona();
        EstadoCliente estadoCliente = new EstadoCliente();

        // Crear una instancia de Cliente
        Cliente cliente = new Cliente();
        cliente.setPassword("secret");
        cliente.setPersona(persona);
        cliente.setEstadoCliente(estadoCliente);

        // Verificar los valores
        assertEquals("secret", cliente.getPassword());
        assertEquals(persona, cliente.getPersona());
        assertEquals(estadoCliente, cliente.getEstadoCliente());
    }

    /**
     * Test de los métodos getter y setter de la entidad Cliente
     */
    @Test
    public void testEquals() {
        Persona persona1 = new Persona();
        EstadoCliente estadoCliente1 = new EstadoCliente();
        Cliente cliente1 = new Cliente("secret", persona1, estadoCliente1);

        Persona persona2 = new Persona();
        EstadoCliente estadoCliente2 = new EstadoCliente();
        Cliente cliente2 = new Cliente("secret", persona2, estadoCliente2);

        // Verificar que dos instancias de Cliente son iguales si tienen los mismos valores
        assertEquals(cliente1, cliente2);
    }

}
