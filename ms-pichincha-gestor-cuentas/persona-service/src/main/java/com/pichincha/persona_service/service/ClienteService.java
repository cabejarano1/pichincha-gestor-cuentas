package com.pichincha.persona_service.service;

import com.pichincha.persona_service.dto.request.ClienteDto;
import com.pichincha.persona_service.dto.request.ClientePatchDto;
import com.pichincha.persona_service.dto.response.IClienteResponse;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface ClienteService {

    CompletableFuture<IClienteResponse> crear(ClienteDto clienteDto);
    CompletableFuture<IClienteResponse> actualizar(Long id, ClienteDto clienteDto);
    CompletableFuture<IClienteResponse> actualizarPorCampos(Long id, ClientePatchDto clienteDto);
    CompletableFuture<Void> eliminarClientePorId(Long id);
    CompletableFuture<IClienteResponse> consultarClientePorId(Long id);
    CompletableFuture<List<IClienteResponse>> consultarClientes();
}