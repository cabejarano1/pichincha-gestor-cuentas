package com.pichincha.persona_service.constants;

public class PathConstants {

    public static final String PERSONA = "persona";
    public static final String ESTADO_CLIENTE = "estado-cliente";
    public static final String CLIENTE = "cliente";
    public static final String ID_PARAM = "/{id}";
}
