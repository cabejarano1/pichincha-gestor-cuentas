package com.pichincha.persona_service.service;

import com.pichincha.persona_service.dto.request.PersonaDto;
import com.pichincha.persona_service.dto.response.IPersonaResponse;
import com.pichincha.persona_service.model.Persona;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface PersonaService {
    CompletableFuture<Persona> crear(PersonaDto personaDto);

    CompletableFuture<Persona> actualizar(Long id, PersonaDto personaDto);

    CompletableFuture<Object> consultarPersonaPorId(Long id, Class<?> type);

    CompletableFuture<Void> eliminar(Long id);

    CompletableFuture<List<IPersonaResponse>> consultarPersonas();
}