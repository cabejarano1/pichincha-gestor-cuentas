package com.pichincha.persona_service.dto.response;

public interface IPersonaResponse {

    Long getId();

    String getNombre();

    String getGenero();

    Integer getEdad();

    String getIdentificacion();

    String getDireccion();

    String getTelefono();
}
