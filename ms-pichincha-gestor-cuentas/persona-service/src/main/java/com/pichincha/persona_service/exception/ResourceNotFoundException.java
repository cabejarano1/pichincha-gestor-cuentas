package com.pichincha.persona_service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Excepción para errores de recurso no encontrado
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException
{

    private String resourceName;
    private String fieldName;
    private Object fieldValue;

    /**
     * Constructor
     * @param resourceName Nombre del recurso
     * @param fieldName Nombre del campo
     * @param fieldValue Valor del campo
     */
    public ResourceNotFoundException(String resourceName, String fieldName, Object fieldValue){
        super(String.format("%s no se encontró con %s = %s", resourceName, fieldName, fieldValue));
        this.resourceName = resourceName;
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
    }

    /**
     * Constructor
     * @param resourceName Nombre del recurso
     */
    public ResourceNotFoundException(String resourceName){
        super(String.format("%s no se encontró con %s = %s", resourceName));
        this.resourceName = resourceName;
    }

}
