package com.pichincha.persona_service.service.implement;

import com.pichincha.persona_service.constants.PathConstants;
import com.pichincha.persona_service.exception.ResourceNotFoundException;
import com.pichincha.persona_service.model.EstadoCliente;
import com.pichincha.persona_service.repository.EstadoClienteRepository;
import com.pichincha.persona_service.service.EstadoClienteService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;

/**
 * Clase para el servicio de estado de cliente
 */
@Service
@RequiredArgsConstructor
public class EstadoClienteServiceImpl implements EstadoClienteService {
    private final EstadoClienteRepository estadoClienteRepository;

    /**
     * Método para consultar un estado de cliente por id
     *
     * @param id Identificador del estado de cliente
     * @return Objeto con la información del estado de cliente
     */
    @Override
    public CompletableFuture<EstadoCliente> consultarEstadoClientePorId(Integer id) {
        return CompletableFuture.supplyAsync(() -> estadoClienteRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(PathConstants.ESTADO_CLIENTE, "id", id)));
    }
}
