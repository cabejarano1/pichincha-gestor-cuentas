package com.pichincha.persona_service.service.implement;

import com.pichincha.persona_service.constants.PathConstants;
import com.pichincha.persona_service.dto.request.PersonaDto;
import com.pichincha.persona_service.dto.response.IPersonaResponse;
import com.pichincha.persona_service.exception.ResourceNotFoundException;
import com.pichincha.persona_service.model.Persona;
import com.pichincha.persona_service.repository.PersonaRepository;
import com.pichincha.persona_service.service.PersonaService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * Clase para el servicio de persona
 */
@Service
@RequiredArgsConstructor
public class PersonaServiceImpl implements PersonaService {

    private final PersonaRepository personaRepository;

    /**
     * Método para crear una persona
     *
     * @param personaDto Objeto con la información de la persona
     * @return Objeto con la información de la persona creada
     */
    @Override
    public CompletableFuture<Persona> crear(PersonaDto personaDto) {
        return validarYGuardarPersona(null, personaDto);
    }

    /**
     * Método para actualizar una persona
     *
     * @param id         Identificador de la persona
     * @param personaDto Objeto con la información de la persona
     * @return Objeto con la información de la persona actualizada
     */
    @Override
    public CompletableFuture<Persona> actualizar(Long id, PersonaDto personaDto) {
        return validarYGuardarPersona(id, personaDto);
    }

    /**
     * Método para actualizar una persona por campos
     *
     * @param id         Identificador de la persona
     * @param personaDto Objeto con la información de la persona
     * @return Objeto con la información de la persona actualizada
     */
    private CompletableFuture<Persona> validarYGuardarPersona(Long id, PersonaDto personaDto) {
        return CompletableFuture.supplyAsync(() -> {
            Persona persona = id != null ? personaRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(PathConstants.PERSONA, "id", id)) : new Persona();
            persona.setNombre(personaDto.getNombre());
            persona.setGenero(personaDto.getGenero());
            persona.setEdad(personaDto.getEdad());
            persona.setIdentificacion(personaDto.getIdentificacion());
            persona.setDireccion(personaDto.getDireccion());
            persona.setTelefono(personaDto.getTelefono());
            return personaRepository.save(persona);
        });
    }

    /**
     * Método para consultar una persona por id
     *
     * @param id Identificador de la persona
     * @return Objeto con la información de la persona
     */
    @Override
    public CompletableFuture<Object> consultarPersonaPorId(Long id, Class<?> type) {
        return CompletableFuture.supplyAsync(() -> {
            Object persona = personaRepository.findById(id, type);
            if (persona == null) throw new ResourceNotFoundException(PathConstants.PERSONA, "id", id);
            return persona;
        });
    }

    /**
     * Método para eliminar una persona
     *
     * @param id Identificador de la persona
     */
    @Override
    public CompletableFuture<Void> eliminar(Long id) {
        return CompletableFuture.runAsync(() -> {
            Persona persona = personaRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(PathConstants.PERSONA, "id", id));
            personaRepository.delete(persona);
        });
    }

    /**
     * Método para consultar todas las personas
     *
     * @return Lista con la información de las personas
     */
    @Override
    public CompletableFuture<List<IPersonaResponse>> consultarPersonas() {
        return CompletableFuture.supplyAsync(() -> personaRepository.findAll(IPersonaResponse.class));
    }
}
