package com.pichincha.persona_service.service.implement;

import com.pichincha.persona_service.constants.PathConstants;
import com.pichincha.persona_service.dto.request.ClienteDto;
import com.pichincha.persona_service.dto.request.ClientePatchDto;
import com.pichincha.persona_service.dto.request.PersonaDto;
import com.pichincha.persona_service.dto.response.IClienteResponse;
import com.pichincha.persona_service.exception.BadRequestException;
import com.pichincha.persona_service.exception.ResourceNotFoundException;
import com.pichincha.persona_service.model.Cliente;
import com.pichincha.persona_service.model.EstadoCliente;
import com.pichincha.persona_service.model.Persona;
import com.pichincha.persona_service.repository.ClienteRepository;
import com.pichincha.persona_service.repository.PersonaRepository;
import com.pichincha.persona_service.service.ClienteService;
import com.pichincha.persona_service.service.EstadoClienteService;
import com.pichincha.persona_service.service.PersonaService;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * Clase para el servicio de cliente
 */
@Service
@RequiredArgsConstructor
public class ClienteServiceImpl implements ClienteService {

    private final ClienteRepository clienteRepository;
    private final PersonaService personaService;
    private final EstadoClienteService estadoClienteService;
    private final PersonaRepository personaRepository;

    /**
     * Método para crear un cliente
     *
     * @param clienteDto Objeto con la información del cliente
     * @return Objeto con la información del cliente creado
     */
    @Override
    public CompletableFuture<IClienteResponse> crear(ClienteDto clienteDto) {
        return validarYGuardarCliente(null, clienteDto);
    }

    /**
     * Método para actualizar un cliente
     *
     * @param id         Identificador del cliente
     * @param clienteDto Objeto con la información del cliente
     * @return Objeto con la información del cliente actualizado
     */
    @Override
    public CompletableFuture<IClienteResponse> actualizar(Long id, ClienteDto clienteDto) {
        return validarYGuardarCliente(id, clienteDto);
    }

    /**
     * Método para actualizar un cliente por campos
     *
     * @param id         Identificador del cliente
     * @param clienteDto Objeto con la información del cliente
     */
    @Override
    public CompletableFuture<IClienteResponse> actualizarPorCampos(Long id, ClientePatchDto clienteDto) {
        return CompletableFuture.supplyAsync(() -> {
            Cliente cliente = clienteRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(PathConstants.CLIENTE, "id", id));
            Persona persona = (Persona) personaService.consultarPersonaPorId(cliente.getId(), Persona.class).join();
            if (clienteDto.getNombre() != null)
                persona.setNombre(clienteDto.getNombre());
            if (clienteDto.getGenero() != null)
                persona.setGenero(clienteDto.getGenero());
            if (clienteDto.getEdad() != null)
                persona.setEdad(clienteDto.getEdad());
            if (clienteDto.getIdentificacion() != null)
                persona.setIdentificacion(clienteDto.getIdentificacion());
            if (clienteDto.getDireccion() != null)
                persona.setDireccion(clienteDto.getDireccion());
            if (clienteDto.getTelefono() != null)
                persona.setTelefono(clienteDto.getTelefono());
            if (clienteDto.getIdEstadoCliente() != null) {
                EstadoCliente estadoCliente = estadoClienteService.consultarEstadoClientePorId(clienteDto.getIdEstadoCliente()).join();
                cliente.setEstadoCliente(estadoCliente);
            }
            if (clienteDto.getPassword() != null)
                cliente.setPassword(clienteDto.getPassword());

            personaRepository.save(persona);
            cliente.setPersona(persona);
            clienteRepository.save(cliente);
            return clienteRepository.findById(cliente.getId(), IClienteResponse.class);
        });
    }

    /**
     * Método para consultar un cliente por id
     *
     * @param id Identificador del cliente
     * @return Objeto con la información del cliente
     */
    @Override
    public CompletableFuture<IClienteResponse> consultarClientePorId(Long id) {
        return CompletableFuture.supplyAsync(() -> {
            IClienteResponse cliente = clienteRepository.findById(id, IClienteResponse.class);
            if (cliente == null)
                throw new ResourceNotFoundException(PathConstants.CLIENTE, "id", id);

            return clienteRepository.findById(id, IClienteResponse.class);
        });
    }

    /**
     * Método para eliminar un cliente por id
     *
     * @param id Identificador del cliente
     */
    @Override
    public CompletableFuture<Void> eliminarClientePorId(Long id) {
        return CompletableFuture.runAsync(() -> {
            Cliente cliente = clienteRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(PathConstants.CLIENTE, "id", id));
            try {
                clienteRepository.deleteById(id);
            } catch (DataIntegrityViolationException e) {
                throw new BadRequestException("Solo se puede eliminar el cliente si no tiene una cuenta asociada");
            }
            personaService.eliminar(cliente.getPersona().getId());
        });
    }

    /**
     * Método para consultar todos los clientes
     *
     * @return Lista con la información de los clientes
     */
    @Override
    public CompletableFuture<List<IClienteResponse>> consultarClientes() {
        return CompletableFuture.supplyAsync(() ->
                clienteRepository.findAll(IClienteResponse.class)
        );
    }

    /**
     * Método para validar y guardar un cliente
     *
     * @param id         Identificador del cliente
     * @param clienteDto Objeto con la información del cliente
     * @return Objeto con la información del cliente
     */
    private CompletableFuture<IClienteResponse> validarYGuardarCliente(Long id, ClienteDto clienteDto) {
        return CompletableFuture.supplyAsync(() -> {
            Cliente cliente = id != null ? clienteRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(PathConstants.CLIENTE, "id", id)) : new Cliente();
            Persona persona = id != null ? (Persona) personaService.consultarPersonaPorId(cliente.getId(), Persona.class).join() : new Persona();
            PersonaDto personaDto = new PersonaDto();
            personaDto.setNombre(clienteDto.getNombre());
            personaDto.setGenero(clienteDto.getGenero());
            personaDto.setEdad(clienteDto.getEdad());
            personaDto.setIdentificacion(clienteDto.getIdentificacion());
            personaDto.setDireccion(clienteDto.getDireccion());
            personaDto.setTelefono(clienteDto.getTelefono());
            persona = id != null ? personaService.actualizar(persona.getId(), personaDto).join() : personaService.crear(personaDto).join();

            // Actualizar el estado del cliente
            EstadoCliente estadoCliente;
            try {
                estadoCliente = estadoClienteService.consultarEstadoClientePorId(clienteDto.getIdEstadoCliente()).join();
            } catch (Exception e) {
                throw new ResourceNotFoundException(PathConstants.ESTADO_CLIENTE, "id", clienteDto.getIdEstadoCliente());
            }
            if (id != null) cliente.setId(id);
            cliente.setPersona(persona);
            cliente.setEstadoCliente(estadoCliente);
            cliente.setPassword(clienteDto.getPassword());
            clienteRepository.save(cliente);
            return clienteRepository.findById(cliente.getId(), IClienteResponse.class);

        });
    }


}
