package com.pichincha.persona_service.repository;

import com.pichincha.persona_service.model.EstadoCliente;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EstadoClienteRepository extends CrudRepository<EstadoCliente, Integer> {
}
