package com.pichincha.persona_service.controller;

import com.pichincha.persona_service.constants.PathConstants;
import com.pichincha.persona_service.dto.request.ClienteDto;
import com.pichincha.persona_service.dto.request.ClientePatchDto;
import com.pichincha.persona_service.dto.response.IClienteResponse;
import com.pichincha.persona_service.exception.BadRequestException;
import com.pichincha.persona_service.service.ClienteService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping(PathConstants.CLIENTE)
@RequiredArgsConstructor
public class ClienteController {

    private final ClienteService clienteService;

    /**
     * Método para crear un cliente
     *
     * @param clienteDto Objeto con la información del cliente
     * @return Objeto con la información del cliente creado
     */
    @PostMapping
    public CompletableFuture<ResponseEntity<IClienteResponse>> crearCliente(@RequestBody @Valid ClienteDto clienteDto) {
        return clienteService.crear(clienteDto).thenApply(ResponseEntity::ok)
                .exceptionally(this::handleException);
    }

    /**
     * Método para actualizar un cliente
     *
     * @param id         Identificador del cliente
     * @param clienteDto Objeto con la información del cliente
     * @return Objeto con la información del cliente actualizado
     */
    @PutMapping(PathConstants.ID_PARAM)
    public CompletableFuture<ResponseEntity<IClienteResponse>> actualizarCliente(@PathVariable Long id, @RequestBody ClienteDto clienteDto) {
        return clienteService.actualizar(id, clienteDto).thenApply(ResponseEntity::ok)
                .exceptionally(this::handleException);
    }

    /**
     * Método para actualizar un cliente por campos
     *
     * @param id         Identificador del cliente
     * @param clienteDto Objeto con la información del cliente
     * @return Objeto con la información del cliente actualizado
     */
    @PatchMapping(PathConstants.ID_PARAM)
    public CompletableFuture<ResponseEntity<IClienteResponse>> actualizarClientePorCampos(@PathVariable Long id, @RequestBody ClientePatchDto clienteDto) {
        return clienteService.actualizarPorCampos(id, clienteDto)
                .thenApply(ResponseEntity::ok)
                .exceptionally(this::handleException);
    }

    /**
     * Método para eliminar un cliente
     *
     * @param id Identificador del cliente
     * @return Respuesta vacía, no content()
     */
    @DeleteMapping(PathConstants.ID_PARAM)
    public CompletableFuture<ResponseEntity<Object>> eliminarCliente(@PathVariable Long id) {
        return clienteService.eliminarClientePorId(id).thenApply(result -> ResponseEntity.noContent().build())
                .exceptionally(this::handleException);
    }

    /**
     * Método para consultar un cliente por id
     *
     * @param id Identificador del cliente
     * @return Objeto con la información del cliente
     */
    @GetMapping(PathConstants.ID_PARAM)
    public CompletableFuture<ResponseEntity<IClienteResponse>> consultarClientePorId(@PathVariable Long id) {
        return clienteService.consultarClientePorId(id).thenApply(ResponseEntity::ok)
                .exceptionally(this::handleException);
    }

    /**
     * Método para consultar todos los clientes
     *
     * @return Lista de objetos con la información de los clientes
     */
    @GetMapping
    public CompletableFuture<ResponseEntity<List<IClienteResponse>>> consultarClientes() {
        return clienteService.consultarClientes().thenApply(ResponseEntity::ok)
                .exceptionally(this::handleException);
    }

    private <T> ResponseEntity<T> handleException(Throwable ex) {
        throw new BadRequestException(ex.getMessage());
    }
}
