package com.pichincha.persona_service.service;

import com.pichincha.persona_service.model.EstadoCliente;

import java.util.concurrent.CompletableFuture;

public interface EstadoClienteService {

    CompletableFuture<EstadoCliente> consultarEstadoClientePorId(Integer id);
}
