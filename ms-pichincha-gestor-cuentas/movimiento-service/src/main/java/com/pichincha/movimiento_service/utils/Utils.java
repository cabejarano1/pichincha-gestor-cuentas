package com.pichincha.movimiento_service.utils;

public class Utils {

    /**
     * Método para determinar si la aplicación está corriendo en Docker
     * @return true si está corriendo en Docker, false si no
     */
    public static boolean estaCorriendoEnDocker() {
            String isDockerEnv = System.getenv("IS_DOCKER");
            return isDockerEnv != null && isDockerEnv.equalsIgnoreCase("true");
    }
}
