package com.pichincha.movimiento_service.controller;


import com.pichincha.movimiento_service.constants.PathConstants;
import com.pichincha.movimiento_service.dto.request.MovimientoDto;
import com.pichincha.movimiento_service.dto.response.IMovimientoResponse;
import com.pichincha.movimiento_service.exception.BadRequestException;
import com.pichincha.movimiento_service.service.implement.MovimientoServiceImpl;
import com.pichincha.movimiento_service.utils.Paginacion;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping(PathConstants.MOVIMIENTO)
@RequiredArgsConstructor
public class MovimientoController {

    private final MovimientoServiceImpl movimientoServiceImpl;

    /**
     * Método para crear un movimiento
     *
     * @param movimientoDto Objeto con la información del movimiento
     * @return Objeto con la información del movimiento creado
     */
    @PostMapping
    public CompletableFuture<ResponseEntity<IMovimientoResponse>> crear(@RequestBody @Valid MovimientoDto movimientoDto) {
        return movimientoServiceImpl.crear(movimientoDto)
                .thenApply(ResponseEntity::ok);
    }

    /**
     * Método para consultar un movimiento por id
     *
     * @param id Identificador del movimiento
     * @return Objeto con la información del movimiento
     */
    @GetMapping(PathConstants.ID_PARAM)
    public CompletableFuture<ResponseEntity<IMovimientoResponse>> consultarMovimientoPorId(@PathVariable Long id) {
        return movimientoServiceImpl.consultarMovimientoPorId(id, IMovimientoResponse.class)
                .thenApply(result -> ResponseEntity.ok((IMovimientoResponse) result))
                .exceptionally(this::handleException);
    }

    /**
     * Método para consultar los movimientos de una cuenta
     *
     * @param id Identificador de la cuenta
     * @return Lista de movimientos de la cuenta
     */
    @GetMapping(PathConstants.CUENTA + PathConstants.ID_PARAM)
    public CompletableFuture<ResponseEntity<List<IMovimientoResponse>>> consultarMovimientoPorIdCuenta(@PathVariable Long id) {
        return movimientoServiceImpl.consultarMovimientosPorCuenta(id)
                .thenApply(ResponseEntity::ok)
                .exceptionally(this::handleException);
    }

    /**
     * Método para consultar los movimientos de una cuenta con paginación
     *
     * @param idCuenta           Identificador de la cuenta
     * @param fechaInicio        Fecha de inicio
     * @param fechaFin           Fecha de fin
     * @param pagina             Página a consultar
     * @param registrosPorPagina Registros por página (tamaño de la página)
     * @param ordenarPor         Ordenar por campo de la entidad
     * @param direccion          Dirección (ASC o DESC)
     * @return Lista de movimientos de la cuenta con paginación
     */
    @GetMapping(PathConstants.ESTADO_CUENTA)
    public CompletableFuture<ResponseEntity<Paginacion<IMovimientoResponse>>> consultarMovimientoPorIdCuenta(
            @RequestParam Long idCuenta,
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime fechaInicio,
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime fechaFin,
            @RequestParam Integer pagina,
            @RequestParam Integer registrosPorPagina,
            @RequestParam String ordenarPor,
            @RequestParam String direccion) {
        if (fechaInicio != null && fechaFin != null) {
            // Si se proporcionan ambas fechas, puedes usarlas para filtrar los movimientos por rango de fechas
            return movimientoServiceImpl.consultarMovimientosPorCuentaYRangoFechas(idCuenta, fechaInicio, fechaFin, pagina, registrosPorPagina, ordenarPor, direccion)
                    .thenApply(ResponseEntity::ok)
                    .exceptionally(this::handleException);
        } else {
            // Si no se proporcionan ambas fechas, puedes simplemente consultar los movimientos por cuenta
            return movimientoServiceImpl.consultarMovimientosPorCuentaYPaginacion(idCuenta, pagina, registrosPorPagina, ordenarPor, direccion)
                    .thenApply(ResponseEntity::ok)
                    .exceptionally(this::handleException);
        }
    }

    /**
     * Método para manejar excepciones
     *
     * @param ex  Excepción
     * @param <T> Tipo de dato
     * @return Excepción personalizada
     */
    private <T> ResponseEntity<T> handleException(Throwable ex) {
        throw new BadRequestException(ex.getMessage());
    }
}


