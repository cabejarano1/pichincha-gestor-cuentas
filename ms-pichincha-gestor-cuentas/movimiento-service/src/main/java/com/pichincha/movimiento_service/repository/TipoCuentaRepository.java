package com.pichincha.movimiento_service.repository;

import com.pichincha.movimiento_service.model.TipoCuenta;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TipoCuentaRepository extends CrudRepository<TipoCuenta, Integer> {
}
