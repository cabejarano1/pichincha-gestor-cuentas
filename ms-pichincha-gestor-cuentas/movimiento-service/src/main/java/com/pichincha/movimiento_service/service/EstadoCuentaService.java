package com.pichincha.movimiento_service.service;

import com.pichincha.movimiento_service.model.EstadoCuenta;

import java.util.concurrent.CompletableFuture;

public interface EstadoCuentaService {
    CompletableFuture<EstadoCuenta> consultarEstadoCuentaPorId(Integer id);

}
