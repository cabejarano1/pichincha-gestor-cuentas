package com.pichincha.movimiento_service.exception.payload;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Clase para manejar la respuesta de una petición


 */
@Data
@NoArgsConstructor
public class ApiResponse {

    private Date tiempo = new Date();
    private String mensaje;
    private String url;

    /**
     * Constructor
     * @param mensaje Mensaje
     * @param url URL
     */
    public ApiResponse(String mensaje, String url) {
        this.mensaje = mensaje;
        this.url = url.replace("uri=","");
    }
}
