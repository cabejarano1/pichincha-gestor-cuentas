package com.pichincha.movimiento_service.service.implement;

import com.pichincha.movimiento_service.constants.PathConstants;
import com.pichincha.movimiento_service.exception.ResourceNotFoundException;
import com.pichincha.movimiento_service.model.TipoCuenta;
import com.pichincha.movimiento_service.repository.TipoCuentaRepository;
import com.pichincha.movimiento_service.service.TipoCuentaService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;

/**
 * Clase para el servicio de tipo de cuenta
 */
@Service
@RequiredArgsConstructor
public class TipoCuentaServiceImpl implements TipoCuentaService {

    private final TipoCuentaRepository repository;

    /**
     * Método para consultar un tipo de cuenta por id
     *
     * @param id Identificador del tipo de cuenta
     * @return Objeto con la información del tipo de cuenta
     */
    @Override
    public CompletableFuture<TipoCuenta> consultarTipoCuentaPorId(Integer id) {
        return CompletableFuture.supplyAsync(() -> repository.findById(id).orElseThrow(() -> new ResourceNotFoundException(PathConstants.TIPO_CUENTA, "id", id)));
    }
}
