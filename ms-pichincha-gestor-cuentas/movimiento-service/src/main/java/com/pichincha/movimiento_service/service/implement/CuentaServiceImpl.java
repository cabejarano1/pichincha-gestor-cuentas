package com.pichincha.movimiento_service.service.implement;

import com.pichincha.movimiento_service.constants.PathConstants;
import com.pichincha.movimiento_service.dto.request.CuentaDto;
import com.pichincha.movimiento_service.dto.request.CuentaPatchDto;
import com.pichincha.movimiento_service.dto.response.ICuentaResponse;
import com.pichincha.movimiento_service.exception.BadRequestException;
import com.pichincha.movimiento_service.exception.ResourceNotFoundException;
import com.pichincha.movimiento_service.model.Cliente;
import com.pichincha.movimiento_service.model.Cuenta;
import com.pichincha.movimiento_service.repository.CuentaRepository;
import com.pichincha.movimiento_service.service.CuentaService;
import com.pichincha.movimiento_service.service.EstadoCuentaService;
import com.pichincha.movimiento_service.service.TipoCuentaService;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import static com.pichincha.movimiento_service.utils.Utils.estaCorriendoEnDocker;

@Service
@RequiredArgsConstructor
public class CuentaServiceImpl implements CuentaService {

    private final CuentaRepository cuentaRepository;
    private final TipoCuentaService tipoCuentaService;
    private final EstadoCuentaService estadoCuentaService;
    private final RestTemplate restTemplate;

    /**
     * Método para crear una cuenta
     *
     * @param cuentaDto Objeto con la información de la cuenta
     * @return Objeto con la información de la cuenta creada
     */
    @Override
    public CompletableFuture<Cuenta> crear(CuentaDto cuentaDto) {
        return validarYGuardarCuenta(null, cuentaDto);
    }

    /**
     * Método para actualizar una cuenta
     *
     * @param id        Identificador de la cuenta
     * @param cuentaDto Objeto con la información de la cuenta
     * @return Objeto con la información de la cuenta actualizada
     */
    @Override
    public CompletableFuture<Cuenta> actualizar(Long id, CuentaDto cuentaDto) {
        return validarYGuardarCuenta(id, cuentaDto);
    }

    /**
     * Método para actualizar una cuenta por campos
     *
     * @param id        Identificador de la cuenta
     * @param cuentaDto Objeto con la información de la cuenta
     * @return Objeto con la información de la cuenta actualizada
     */
    @Override
    public CompletableFuture<ICuentaResponse> actualizarPorCampos(Long id, CuentaPatchDto cuentaDto) {
        return CompletableFuture.supplyAsync(() -> {
            Cuenta cuenta = cuentaRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(PathConstants.CUENTA, "id", id));

            if (cuentaDto.getIdCliente() != null)
                cuenta.setCliente(consultarClientePorId(cuentaDto.getIdCliente()));
            if (cuentaDto.getNumeroCuenta() != null)
                cuenta.setNumeroCuenta(cuentaDto.getNumeroCuenta());
            if (cuentaDto.getIdTipoMovimiento() != null)
                cuenta.setTipoCuenta(tipoCuentaService.consultarTipoCuentaPorId(cuentaDto.getIdTipoMovimiento()).join());
            if (cuentaDto.getSaldoInicial() != null)
                cuenta.setSaldoInicial(cuentaDto.getSaldoInicial());
            if (cuentaDto.getIdEstadoCuenta() != null)
                cuenta.setEstadoCuenta(estadoCuentaService.consultarEstadoCuentaPorId(cuentaDto.getIdEstadoCuenta()).join());

            cuentaRepository.save(cuenta);
            return cuentaRepository.findById(cuenta.getId(), ICuentaResponse.class);
        });
    }

    /**
     * Método para consultar una cuenta por id
     *
     * @param id    Identificador de la cuenta
     * @param clase Clase de la entidad
     * @return Objeto con la información de la cuenta
     */
    @Override
    public CompletableFuture<Object> consultarCuentaPorId(Long id, Class<?> clase) {
        return CompletableFuture.supplyAsync(() -> {
            Object cuenta = cuentaRepository.findById(id, clase);
            if (cuenta == null)
                throw new ResourceNotFoundException(PathConstants.CUENTA, "id", id);
            return cuenta;
        });
    }

    /**
     * Método para consultar todas las cuentas
     *
     * @return Lista con la información de las cuentas
     */
    @Override
    public CompletableFuture<List<ICuentaResponse>> consultarCuentas() {
        return CompletableFuture.supplyAsync(() -> cuentaRepository.findAll(ICuentaResponse.class));
    }

    /**
     * Método para consultar todas las cuentas de un cliente
     *
     * @param id Identificador del cliente
     * @return Lista con la información de las cuentas
     */
    private CompletableFuture<Cuenta> validarYGuardarCuenta(Long id, CuentaDto cuentaDto) {
        return CompletableFuture.supplyAsync(() -> {
            Cuenta cuenta = id != null ? cuentaRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(PathConstants.CUENTA, "id", id)) : new Cuenta();
            if (cuenta.getCliente() != null && cuenta.getCliente().getId() != cuentaDto.getIdCliente() && cuentaRepository.existsByCliente_Id(cuentaDto.getIdCliente()))
                throw new BadRequestException("El cliente ya tiene una cuenta asociada");

            if (cuenta.getCliente() == null && cuentaRepository.existsByCliente_Id(cuentaDto.getIdCliente()))
                throw new BadRequestException("El cliente ya tiene una cuenta asociada");

            Cliente cliente = consultarClientePorId(cuentaDto.getIdCliente());
            if (cliente == null)
                throw new ResourceNotFoundException(PathConstants.CLIENTE, "id", cuentaDto.getIdCliente());

            cuenta.setCliente(cliente);
            cuenta.setNumeroCuenta(cuentaDto.getNumeroCuenta());
            cuenta.setTipoCuenta(tipoCuentaService.consultarTipoCuentaPorId(cuentaDto.getIdTipoCuenta()).join());
            cuenta.setSaldoInicial(cuentaDto.getSaldoInicial());
            cuenta.setEstadoCuenta(estadoCuentaService.consultarEstadoCuentaPorId(cuentaDto.getIdEstadoCuenta()).join());
            return cuentaRepository.save(cuenta);
        });
    }

    /**
     * Método con circuit breaker para consultar un cliente por id
     * @param id Identificador del cliente
     * @return Objeto con la información del cliente
     */
    @CircuitBreaker(name = "persona-service", fallbackMethod = "fallbackConsultarClientePorId")
    private Cliente consultarClientePorId(Long id) {
        try {
            String url = estaCorriendoEnDocker() ? PathConstants.DOCKER_HOST_PERSONA : PathConstants.LOCAL_HOST_PERSONA;
            url = url + PathConstants.CLIENTE + "/" + id;
            return restTemplate.getForObject(url, Cliente.class);
        } catch (HttpClientErrorException ex) {
            if (ex.getStatusCode() == HttpStatus.NOT_FOUND) {
                throw new ResourceNotFoundException(PathConstants.CLIENTE, "id", id);
            } else {
                throw ex;
            }
        }
    }

    /**
     * Método para manejar el fallback de consultar un cliente por id
     * @param id Identificador del cliente
     * @param ex Excepción lanzada
     * @return Objeto con la información del cliente
     */
    public Cliente fallbackConsultarClientePorId(Long id, Exception ex) throws ResourceNotFoundException {
        throw new BadRequestException("Servicio de consulta de cliente no disponible");
    }

}
