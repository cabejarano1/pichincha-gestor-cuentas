package com.pichincha.movimiento_service.enums;

import lombok.Getter;

/**
 * Enumeración que contiene los tipos de movimiento
 */
@Getter
public enum TipoMovimientoEnum {
    DEPOSITO(1, "Depósito", true),
    RETIRO(2 , "Retiro", false),
    TRANSFERENCIA_RECEPCIONADA(3, "Transferencia recibida", true),
    TRANSFERENCIA_ENVIADA(4, "Transferencia enviada", false);

    private final Integer id;
    private final String nombre;
    private final Boolean esIngreso;

    /**
     * Constructor de la clase
     * @param id id del tipo de movimiento
     * @param nombre nombre del tipo de movimiento
     * @param esIngreso indica si el movimiento es un ingreso
     */
    TipoMovimientoEnum(Integer id, String nombre, Boolean esIngreso) {
        this.id = id;
        this.nombre = nombre;
        this.esIngreso = esIngreso;
    }

    /**
     * Método que permite obtener el nombre del tipo de movimiento por id
     * @param id id del tipo de movimiento
     * @return nombre del tipo de movimiento
     */
    public static Boolean esIngresoPorId(Integer id){
        for (TipoMovimientoEnum tipoMovimientoEnum : TipoMovimientoEnum.values()) {
            if(tipoMovimientoEnum.getId().equals(id)){
                return tipoMovimientoEnum.getEsIngreso();
            }
        }
        return null;
    }
}
