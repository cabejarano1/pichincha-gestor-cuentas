package com.pichincha.movimiento_service.service;

import com.pichincha.movimiento_service.model.TipoCuenta;

import java.util.concurrent.CompletableFuture;

public interface TipoCuentaService {

    CompletableFuture<TipoCuenta> consultarTipoCuentaPorId(Integer id);
}
