package com.pichincha.movimiento_service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Excepción para errores de petición incorrecta

 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class BadRequestException extends RuntimeException {

    /**
     * Constructor
     * @param message Mensaje de error
     */
    public BadRequestException(String message) {
        super(message);
    }
}
