package com.pichincha.movimiento_service.service.implement;

import com.pichincha.movimiento_service.constants.PathConstants;
import com.pichincha.movimiento_service.exception.ResourceNotFoundException;
import com.pichincha.movimiento_service.model.EstadoCuenta;
import com.pichincha.movimiento_service.repository.EstadoCuentaRepository;
import com.pichincha.movimiento_service.service.EstadoCuentaService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;

/**
 * Clase para el servicio de estado de cuenta
 */
@Service
@RequiredArgsConstructor
public class EstadoCuentaServiceImpl implements EstadoCuentaService {

    private final EstadoCuentaRepository estadoCuentaRepository;

    /**
     * Método para consultar un estado de cuenta por id
     *
     * @param id Identificador del estado de cuenta
     * @return Objeto con la información del estado de cuenta
     */
    @Override
    public CompletableFuture<EstadoCuenta> consultarEstadoCuentaPorId(Integer id) {
        return CompletableFuture.supplyAsync(() -> estadoCuentaRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(PathConstants.ESTADO_CUENTA, "id", id)));
    }
}
