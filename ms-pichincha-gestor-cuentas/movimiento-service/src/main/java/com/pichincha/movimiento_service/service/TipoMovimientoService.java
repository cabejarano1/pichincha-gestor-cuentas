package com.pichincha.movimiento_service.service;

import com.pichincha.movimiento_service.model.TipoMovimiento;

import java.util.concurrent.CompletableFuture;

public interface TipoMovimientoService {

    CompletableFuture<TipoMovimiento> consultarTipoMovimientoPorId(Integer id);
}
