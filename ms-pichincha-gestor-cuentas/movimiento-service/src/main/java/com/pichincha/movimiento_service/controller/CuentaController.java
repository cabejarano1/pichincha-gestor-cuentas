package com.pichincha.movimiento_service.controller;

import com.pichincha.movimiento_service.constants.PathConstants;
import com.pichincha.movimiento_service.dto.request.CuentaDto;
import com.pichincha.movimiento_service.dto.request.CuentaPatchDto;
import com.pichincha.movimiento_service.dto.response.ICuentaResponse;
import com.pichincha.movimiento_service.model.Cuenta;
import com.pichincha.movimiento_service.service.implement.CuentaServiceImpl;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping(PathConstants.CUENTA)
@RequiredArgsConstructor
public class CuentaController {

    private final CuentaServiceImpl cuentaServiceImpl;

    /**
     * Método para crear una cuenta
     *
     * @param cuentaDto Objeto con la información de la cuenta
     * @return Objeto con la información de la cuenta creada
     */
    @PostMapping
    public CompletableFuture<ResponseEntity<Cuenta>> crearCuenta(@RequestBody @Valid CuentaDto cuentaDto) {
        return cuentaServiceImpl.crear(cuentaDto).thenApply(ResponseEntity::ok);
    }

    /**
     * Método para actualizar una cuenta
     *
     * @param id         Identificador de la cuenta
     * @param clienteDto Objeto con la información de la cuenta
     * @return Objeto con la información de la cuenta actualizada
     */
    @PutMapping(PathConstants.ID_PARAM)
    public CompletableFuture<ResponseEntity<Cuenta>> actualizarCuenta(@PathVariable Long id, @RequestBody @Valid CuentaDto clienteDto) {
        return cuentaServiceImpl.actualizar(id, clienteDto).thenApply(ResponseEntity::ok);
    }

    /**
     * Método para actualizar una cuenta por campos
     *
     * @param id         Identificador de la cuenta
     * @param clienteDto Objeto con la información de la cuenta
     * @return Objeto con la información de la cuenta actualizada
     */
    @PatchMapping(PathConstants.ID_PARAM)
    public CompletableFuture<ResponseEntity<ICuentaResponse>> actualizarCuentaPorCampos(@PathVariable Long id, @RequestBody CuentaPatchDto clienteDto) {
        return cuentaServiceImpl.actualizarPorCampos(id, clienteDto)
                .thenApply(ResponseEntity::ok);
    }

    /**
     * Método para consultar una cuenta por id
     *
     * @param id Identificador de la cuenta
     * @return Objeto con la información de la cuenta
     */
    @GetMapping(PathConstants.ID_PARAM)
    public CompletableFuture<ResponseEntity<ICuentaResponse>> consultarCuentaPorId(@PathVariable Long id) {
        return cuentaServiceImpl.consultarCuentaPorId(id, ICuentaResponse.class).thenApply(result -> ResponseEntity.ok((ICuentaResponse) result));
    }

    /**
     * Método para consultar cuentas
     *
     * @return Lista de objetos con la información de las cuentas
     */
    @GetMapping
    public CompletableFuture<ResponseEntity<List<ICuentaResponse>>> consultarCuentas() {
        return cuentaServiceImpl.consultarCuentas().thenApply(ResponseEntity::ok);
    }

}


