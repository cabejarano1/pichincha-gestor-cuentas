package com.pichincha.movimiento_service.utils;

import lombok.Data;

import java.util.List;

/**
 * Clase para la paginación de listas
 * @param <T> Tipo de objeto de la lista
 */
@Data
public class Paginacion<T> {
    private List<T> lista;
    private Integer pagina;
    private Integer limite;
    private Integer totalRegistros;
    private Integer totalPaginas;

    /**
     * Constructor de la clase
     * @param lista Lista de objetos
     * @param pagina Número de página
     * @param limite Número de registros por página
     * @param totalRegistros Número total de registros
     */
    public Paginacion(List<T> lista, Integer pagina, Integer limite, Integer totalRegistros) {
        this.lista = lista;
        this.pagina = pagina;
        this.limite = limite;
        this.totalRegistros = totalRegistros;
        this.totalPaginas = (int) Math.ceil((double) totalRegistros / limite);
    }

}
