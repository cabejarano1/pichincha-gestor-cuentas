package com.pichincha.movimiento_service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MovimientoServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(MovimientoServiceApplication.class, args);
    }

}
