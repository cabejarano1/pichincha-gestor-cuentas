package com.pichincha.movimiento_service.constants;

public class PathConstants {

    public static final String LOCAL_HOST_PERSONA = "http://localhost:8081";
    public static final String DOCKER_HOST_PERSONA = "http://persona-service:8081";
    public static final String CLIENTE = "/cliente";
    public static final String CUENTA = "/cuenta";
    public static final String TIPO_MOVIMIENTO = "/tipo-movimiento";
    public static final String ESTADO_CUENTA = "/estado-cuenta";
    public static final String ID_PARAM = "/{id}";
    public static final String TIPO_CUENTA = "/tipo-cuenta";
    public static final String MOVIMIENTO = "/movimiento";
}
