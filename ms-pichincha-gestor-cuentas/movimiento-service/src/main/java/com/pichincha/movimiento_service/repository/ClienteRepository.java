package com.pichincha.movimiento_service.repository;

import com.pichincha.movimiento_service.model.Cliente;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClienteRepository extends CrudRepository<Cliente, Long>{
}
