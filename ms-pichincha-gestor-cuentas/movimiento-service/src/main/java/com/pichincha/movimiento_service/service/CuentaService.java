package com.pichincha.movimiento_service.service;

import com.pichincha.movimiento_service.dto.request.CuentaDto;
import com.pichincha.movimiento_service.dto.request.CuentaPatchDto;
import com.pichincha.movimiento_service.dto.response.ICuentaResponse;
import com.pichincha.movimiento_service.model.Cuenta;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface CuentaService {
    CompletableFuture<Cuenta> crear(CuentaDto cuentaDto);
    CompletableFuture<Cuenta> actualizar(Long id, CuentaDto cuentaDto);
    CompletableFuture<ICuentaResponse> actualizarPorCampos(Long id, CuentaPatchDto cuentaDto);
    CompletableFuture<Object> consultarCuentaPorId(Long id, Class<?> clase);
    CompletableFuture<List<ICuentaResponse>> consultarCuentas();
}
