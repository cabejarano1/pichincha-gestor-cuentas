package com.pichincha.movimiento_service.service.implement;

import com.pichincha.movimiento_service.constants.PathConstants;
import com.pichincha.movimiento_service.exception.ResourceNotFoundException;
import com.pichincha.movimiento_service.model.TipoMovimiento;
import com.pichincha.movimiento_service.repository.TipoMovimientoRepository;
import com.pichincha.movimiento_service.service.TipoMovimientoService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;

@Service
@RequiredArgsConstructor
public class TipoMovimientoServiceImpl implements TipoMovimientoService {

    private final TipoMovimientoRepository tipoMovimientoRepository;

    /**
     * Método para consultar un tipo de movimiento por id
     *
     * @param id Identificador del tipo de movimiento
     * @return Objeto con la información del tipo de movimiento
     */
    public CompletableFuture<TipoMovimiento> consultarTipoMovimientoPorId(Integer id) {
        return CompletableFuture.supplyAsync(() -> tipoMovimientoRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(PathConstants.TIPO_MOVIMIENTO, "id", id)));
    }
}
