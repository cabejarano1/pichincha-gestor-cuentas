package com.pichincha.movimiento_service.repository;

import com.pichincha.movimiento_service.model.EstadoCuenta;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EstadoCuentaRepository extends CrudRepository<EstadoCuenta, Integer>{
}
