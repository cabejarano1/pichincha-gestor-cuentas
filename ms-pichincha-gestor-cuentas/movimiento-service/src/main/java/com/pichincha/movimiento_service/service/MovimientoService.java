package com.pichincha.movimiento_service.service;

import com.pichincha.movimiento_service.dto.request.MovimientoDto;
import com.pichincha.movimiento_service.dto.response.IMovimientoResponse;
import com.pichincha.movimiento_service.utils.Paginacion;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface MovimientoService {
    CompletableFuture<IMovimientoResponse> crear(MovimientoDto movimientoDto);

    CompletableFuture<Object> consultarMovimientoPorId(Long id, Class<?> clase);

    CompletableFuture<List<IMovimientoResponse>> consultarMovimientosPorCuenta(Long idCuenta);

    CompletableFuture<Paginacion<IMovimientoResponse>> consultarMovimientosPorCuentaYPaginacion(Long idCuenta, Integer pagina, Integer registrosPorPagina, String ordenarPor, String direccion);

    CompletableFuture<Paginacion<IMovimientoResponse>> consultarMovimientosPorCuentaYRangoFechas(Long idCuenta, LocalDateTime fechaInicio, LocalDateTime fechaFin, Integer pagina, Integer registrosPorPagina, String ordenarPor, String direccion);
}
