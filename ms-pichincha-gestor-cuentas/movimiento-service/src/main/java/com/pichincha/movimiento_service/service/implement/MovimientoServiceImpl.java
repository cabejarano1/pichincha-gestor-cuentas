package com.pichincha.movimiento_service.service.implement;

import com.pichincha.movimiento_service.dto.request.MovimientoDto;
import com.pichincha.movimiento_service.dto.response.IMovimientoResponse;
import com.pichincha.movimiento_service.enums.TipoMovimientoEnum;
import com.pichincha.movimiento_service.exception.BadRequestException;
import com.pichincha.movimiento_service.exception.ResourceNotFoundException;
import com.pichincha.movimiento_service.model.Cuenta;
import com.pichincha.movimiento_service.model.Movimiento;
import com.pichincha.movimiento_service.model.TipoMovimiento;
import com.pichincha.movimiento_service.repository.MovimientoRepository;
import com.pichincha.movimiento_service.service.CuentaService;
import com.pichincha.movimiento_service.service.MovimientoService;
import com.pichincha.movimiento_service.service.TipoMovimientoService;
import com.pichincha.movimiento_service.utils.Paginacion;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * Clase para el servicio de movimiento
 */
@Service
@RequiredArgsConstructor
public class MovimientoServiceImpl implements MovimientoService {

    private final MovimientoRepository movimientoRepository;
    private final CuentaService cuentaService;
    private final TipoMovimientoService tipoMovimientoService;

    /**
     * Método para crear un movimiento
     *
     * @param movimientoDto Objeto con la información del movimiento
     * @return Objeto con la información del movimiento creado
     */
    @Override
    public CompletableFuture<IMovimientoResponse> crear(MovimientoDto movimientoDto) {
        return CompletableFuture.supplyAsync(() -> {
            Cuenta cuenta = (Cuenta) cuentaService.consultarCuentaPorId(movimientoDto.getIdCuenta(), Cuenta.class).join();
            TipoMovimiento tipoMovimiento = tipoMovimientoService.consultarTipoMovimientoPorId(movimientoDto.getIdTipoMovimiento()).join();
            if (cuenta == null)
                throw new ResourceNotFoundException("Cuenta", "id", movimientoDto.getIdCuenta());
            Movimiento ultimoMovimiento = movimientoRepository.findLastMovimientoByCuenta(cuenta);
            BigDecimal saldo = ultimoMovimiento != null ? ultimoMovimiento.getSaldo() : cuenta.getSaldoInicial();
            Boolean esIngreso = TipoMovimientoEnum.esIngresoPorId(tipoMovimiento.getId());
            //Compara el saldo que se tiene en la cuenta con el del movimiento (movimientoDto.getValor()) si el movimiento es mayor al que se tiene en la cuenta no permitira la transaccion si es que no es ingreso
            if (Boolean.FALSE.equals(esIngreso) && saldo.compareTo(movimientoDto.getValor()) < 0)
                throw new BadRequestException("Saldo no disponible");

            Movimiento movimiento = new Movimiento();
            movimiento.setCuenta(cuenta);
            movimiento.setFecha(LocalDateTime.now());
            movimiento.setTipoMovimiento(tipoMovimiento);
            movimiento.setValor(movimientoDto.getValor());
            movimiento.setSaldo(Boolean.TRUE.equals(esIngreso) ? saldo.add(movimientoDto.getValor()) : saldo.subtract(movimientoDto.getValor()));
            movimiento.setEstado(Boolean.TRUE);
            movimientoRepository.save(movimiento);
            return movimientoRepository.findById(movimiento.getId(), IMovimientoResponse.class);
        });

    }

    /**
     * Método para consultar un movimiento por id
     *
     * @param id    Identificador del movimiento
     * @param clase Clase del objeto a retornar
     * @return Objeto con la información del movimiento
     */
    @Override
    public CompletableFuture<Object> consultarMovimientoPorId(Long id, Class<?> clase) {
        return CompletableFuture.supplyAsync(() -> movimientoRepository.findById(id, clase));
    }

    /**
     * Método para consultar los movimientos por cuenta
     *
     * @param idCuenta Identificador de la cuenta
     * @return Lista con la información de los movimientos
     */
    @Override
    public CompletableFuture<List<IMovimientoResponse>> consultarMovimientosPorCuenta(Long idCuenta) {
        return CompletableFuture.supplyAsync(() -> movimientoRepository.findByCuentaId(idCuenta, IMovimientoResponse.class));
    }

    /**
     * Método para consultar los movimientos por cuenta y paginación
     *
     * @param idCuenta           Identificador de la cuenta
     * @param pagina             Número de página
     * @param registrosPorPagina Número de registros por página
     * @param ordenarPor         Campo por el cual se ordenará
     * @param direccion          Dirección de la ordenación
     * @return Objeto con la información de los movimientos paginados
     */
    @Override
    public CompletableFuture<Paginacion<IMovimientoResponse>> consultarMovimientosPorCuentaYPaginacion(Long idCuenta, Integer pagina, Integer registrosPorPagina, String ordenarPor, String direccion) {
        return CompletableFuture.supplyAsync(() -> {
            Pageable pageable = crearObjetoPaginacion(pagina, registrosPorPagina, ordenarPor, direccion);
            Long totalRegistros = movimientoRepository.countByCuentaId(idCuenta);
            List<IMovimientoResponse> lista = movimientoRepository.findByCuentaId(idCuenta, IMovimientoResponse.class, pageable);
            return new Paginacion<>(lista, pagina, registrosPorPagina, totalRegistros.intValue());
        });
    }

    /**
     * Método para consultar los movimientos por cuenta y rango de fechas
     *
     * @param idCuenta    Identificador de la cuenta
     * @param fechaInicio Fecha de inicio
     * @param fechaFin    Fecha de fin
     * @return Lista con la información de los movimientos
     */
    @Override
    public CompletableFuture<Paginacion<IMovimientoResponse>> consultarMovimientosPorCuentaYRangoFechas(Long idCuenta, LocalDateTime fechaInicio, LocalDateTime fechaFin, Integer pagina, Integer registrosPorPagina, String ordenarPor, String direccion) {
        return CompletableFuture.supplyAsync(() -> {
            Pageable pageable = crearObjetoPaginacion(pagina, registrosPorPagina, ordenarPor, direccion);
            Long totalRegistros = movimientoRepository.countByCuentaIdAndFechaBetweenAndEstado(idCuenta, fechaInicio, fechaFin, Boolean.TRUE);
            List<IMovimientoResponse> lista = movimientoRepository.findByCuentaIdAndFechaBetweenAndEstado(idCuenta, fechaInicio, fechaFin, Boolean.TRUE, IMovimientoResponse.class, pageable);
            return new Paginacion<>(lista, pagina, registrosPorPagina, totalRegistros.intValue());
        });
    }

    /**
     * Método para crear un objeto de paginación
     *
     * @param pagina             Número de página
     * @param registrosPorPagina Número de registros por página
     * @param ordenarPor         Campo por el cual se ordenará
     * @param direccion          Dirección de la ordenación
     * @return Objeto de paginación
     */
    private Pageable crearObjetoPaginacion(Integer pagina, Integer registrosPorPagina, String ordenarPor, String direccion) {
        try {
            Sort sort = ordenarPor != null && direccion != null ? Sort.by(Sort.Direction.fromString(direccion), ordenarPor) : Sort.unsorted();
            return PageRequest.of(pagina - 1, registrosPorPagina, sort);
        } catch (Exception e) {
            throw new BadRequestException("Error al consultar los movimientos, revise los parámetros de consulta");
        }
    }
}
